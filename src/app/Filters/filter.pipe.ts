import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(products: any, term: any): any {
     if(term === undefined || term == 0) return products;
     return products.filter(function (product) {
         return product[1].toLowerCase().includes(term.toLowerCase());
     })
  }

}
