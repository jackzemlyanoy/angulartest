import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterIndex'
})
export class FilterIndexPipe implements PipeTransform {

    transform(products: any, filterIndex: any): any {
        if(filterIndex === undefined || filterIndex==0) return products;
        return products.filter(function (product:number) {
            return product[0].toString().toLowerCase().includes(filterIndex.toLowerCase());
        })
    }

}
