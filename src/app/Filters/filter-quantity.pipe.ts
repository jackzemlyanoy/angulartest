import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterQuantity'
})
export class FilterQuantityPipe implements PipeTransform {

  transform(products: any, filterQuantity: any): any {
      if(filterQuantity === undefined || filterQuantity == 0) return products;
      return products.filter(function (product) {
          return product[3].toString().toLowerCase().includes(filterQuantity.toLowerCase());
      })
  }

}
