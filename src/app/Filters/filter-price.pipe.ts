import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPrice'
})
export class FilterPricePipe implements PipeTransform {

  transform(products: any, filterPrice: any): any {
      if(filterPrice === undefined || filterPrice == 0) return products;
      return products.filter(function (product) {
          return product[2].toLowerCase().includes(filterPrice.toLowerCase());
      })
  }

}
