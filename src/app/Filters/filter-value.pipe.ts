import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterValue'
})
export class FilterValuePipe implements PipeTransform {
  transform(products: any, filterValue: any): any {
      if(filterValue === undefined || filterValue == 0) return products;
      return products.filter(function (product) {
          return product[4].toLowerCase().includes(filterValue.toLowerCase());
      });
  }
}
