import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterId'
})
export class FilterIdPipe implements PipeTransform {

      transform(products: any, filterId: any): any {
      if(filterId === undefined || filterId==0) return products;
      return products.filter(function (product:number) {
          return product[0].toString().toLowerCase().includes(filterId.toLowerCase());
      })
  }
}
