///<reference path="../Filters/filter.pipe.ts"/>
import {Component, OnInit, Input} from '@angular/core';
import { ProductService, Product } from "../shared/index";
import { FilterPipe } from "../Filters/filter.pipe";
import { FilterIdPipe } from "../Filters/filter-id.pipe";
import { FilterPricePipe } from "../Filters/filter-price.pipe";
import { FilterQuantityPipe } from "../Filters/filter-quantity.pipe";
import { FilterValuePipe } from "../Filters/filter-value.pipe";
import { FilterIndexPipe } from "../Filters/filter-index.pipe";
import { AngularFontAwesomeModule } from 'angular-font-awesome';


@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.css',
        '../../../node_modules/font-awesome/css/font-awesome.css'
    ]
})
export class TableComponent implements OnInit {

    copyProducts: Product[] = [];
    products: Product[];
    showId: boolean = false;
    showName: boolean = false;
    showQuantity: boolean = false;
    showPrice: boolean = false;
    showValue: boolean = false;
    showIndex: boolean = false;
    showTr: boolean = false;
    tableHeader: any;
    p: number[] = [];
    newTtableHeader: any;
    showButtons: boolean;
    @Input() tbIndex: number;
    mIndex: number;

    constructor(private service: ProductService) {
    }

    private getData(url) {
        this.service.getProducts(url).subscribe(
            (items) => {
                this.tableHeader = items.shift();
                this.products = items;
                this.copyProducts = items;
                this.showButtons = false;
            },
            // (items)=>{
            //     if(items.length == undefined){
            //         alert("Can`t get data");
            //     }
            // }
            );
    }

    sortType(sort: string) {
        if (sort === 'index' && this.showIndex == false) {
            this.products = this.copyProducts.sort(this.compareValues([0], 'desc'))
        }
        if (sort === 'index' && this.showIndex == true) {
            this.products = this.copyProducts.sort(this.compareValues([0]))
        }
        if (sort === 'name' && this.showName === false) {
            this.products = this.copyProducts.sort(this.sortByName);
        }
        if (sort === 'name' && this.showName === true) {
            this.products = this.copyProducts.sort(this.sortByNameRevers);
        }
        if (sort === 'id' && this.showId == false) {
            this.products = this.copyProducts.sort(this.compareValues([0], 'desc'))
        }
        if (sort === 'id' && this.showId == true) {
            this.products = this.copyProducts.sort(this.compareValues([0]))
        }
        if (sort === 'quantity' && this.showQuantity == false) {
            this.products = this.copyProducts.sort(this.sortByQuantity)
        }
        if (sort === 'quantity' && this.showQuantity == true) {
            this.products = this.copyProducts.sort(this.sortByQuantityRevers)
        }
        if (sort === 'price' && this.showPrice == false) {
            this.products = this.copyProducts.sort(this.compareValuesPrice([2], 'desc'))
        }
        if (sort === 'price' && this.showPrice == true) {
            this.products = this.copyProducts.sort(this.compareValuesPrice([2]))
        }
        if (sort === 'value' && this.showValue == false) {
            this.products = this.copyProducts.sort(this.compareValues([4], 'desc'))
        }
        if (sort === 'value' && this.showValue == true) {
            this.products = this.copyProducts.sort(this.compareValues([4]))
        }
    }

    toggleIndex() {
        this.showIndex = !this.showIndex;
    }

    toggleId() {
        this.showId = !this.showId;
    }

    toggleName() {
        this.showName = !this.showName;
    }

    toggleQuantity() {
        this.showQuantity = !this.showQuantity;
    }
    togglePrice() {
        this.showPrice = !this.showPrice;
    }
    toggleValue() {
        this.showValue = !this.showValue;
    }

    compareValues(key, order = 'asc') {
        return function (a, b) {

            const varA = (typeof a[key] === 'string')
                ? a[key].toUpperCase()
                : a[key];
            const varB = (typeof b[key] === 'string')
                ? b[key].toUpperCase()
                : b[key];
            let comparison = (varA > varB)
                ? 1
                : (varA < varB)
                    ? -1
                    : 0;

            return (order === 'desc') ? (comparison * -1) : comparison;
        };
    }

    compareValuesPrice(key, order = 'asc') {
        return function (a, b) {

            const varA = (typeof a[key] === 'string')
                ? a[key]
                : a[key];
            const varB = (typeof b[key] === 'string')
                ? b[key]
                : b[key];
            let comparison = (+varA > +varB)
                ? 1
                : (+varA < +varB)
                    ? -1
                    : 0;

            return (order === 'desc') ? (comparison * -1) : comparison;
        };
    }

    sortByName(a, b) {
        if (a[1] < b[1]) return -1;
        if (a[1] > b[1]) return 1;
        return 0;
    }

    sortByNameRevers(a, b) {
        if (a[1] < b[1]) return 1;
        if (a[1] > b[1]) return -1;
        return 0;
    }

    sortByQuantityRevers(a, b) {
        if (a[3] < b[3]) return 1;
        if (a[3] > b[3]) return -1;
        return 0;
    }

    sortByQuantity(a, b) {
        if (a[3] < b[3]) return -1;
        if (a[3] > b[3]) return 1;
        return 0;
    }

    getProductsId(i) {
        this.newTtableHeader = this.copyProducts;
        this.newTtableHeader = i;
    }

    toggleTr() {
        this.showTr = true;
    }

    ngOnInit() {
        this.mIndex = this.tbIndex;
        this.showButtons = true;
    }

}
