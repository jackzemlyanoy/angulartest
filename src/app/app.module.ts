import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { routes } from './app.routes';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProductService } from './shared/index';
import { TableComponent } from './table/table.component';
import { FilterPipe } from './Filters/filter.pipe';
import { FilterIdPipe } from './Filters/filter-id.pipe';
import { FilterPricePipe } from './Filters/filter-price.pipe';
import { FilterQuantityPipe } from './Filters/filter-quantity.pipe';
import { FilterValuePipe } from './Filters/filter-value.pipe';
import { FilterIndexPipe } from './Filters/filter-index.pipe';

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        FormsModule,
        AngularFontAwesomeModule,
        RouterModule.forRoot(routes)
    ],
    declarations: [
        AppComponent,
        TableComponent,
        FilterPipe,
        FilterIdPipe,
        FilterPricePipe,
        FilterQuantityPipe,
        FilterValuePipe,
        FilterIndexPipe,
    ],
    bootstrap: [AppComponent],
    providers: [ProductService]
})
export class AppModule {
}
