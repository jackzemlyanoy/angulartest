import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from "./table/table.component";

export const routes: Routes = [
    { path: '', loadChildren: './table/table/table.module#TableModule'}
    ];
