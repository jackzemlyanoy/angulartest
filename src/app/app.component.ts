import {Component} from '@angular/core';
import './rx-js.operators';

@Component({
    moduleId: module.id,
    selector: 'app-my',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.css']
})
export class AppComponent {

    tables: any = [];
    tbIndex: number = 1;

    constructor() {
    }

    toggleAdd() {
        this.tables.push(this.tbIndex);
        this.tbIndex++;
    }

}
