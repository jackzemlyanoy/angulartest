export class Product {
    public id: string;
    public name: string;
    public price: string;
    public quantity: number;
    public value: string;

    constructor(id, name, price, quantity, value) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.value = value;
    }
}

