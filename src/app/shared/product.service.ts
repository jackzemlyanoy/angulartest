import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/internal/operators'

@Injectable()
export class ProductService {
    // адрес сервиса
    private url = 'http://localhost:4200/assets/';

    constructor(private http: HttpClient) {
    }

    // Отправка GET запроса нв сервер
    public getProducts(varUrl) {
        return this.http.get('./assets/' + varUrl + '.json')
            .pipe(
                // catchError(this.handleError),
                map((result: any) => result)
            )
    }
}
